package week3.assignment_abstarction;

abstract class Shapes{
    abstract void area();
}
class Circle extends Shapes{
    void area(){
        float r=2;
        double area=(3.14*r*r);
        System.out.println("area of Circle is "+area);
    }
}
class Square extends Shapes{
    void area(){
        float side=2;
        double area=side*side;
        System.out.println("area of Square is "+area);
    }
}
class Cylinder extends Circle{
    void area(){
        float height=2;
        float r=3;
        double area=2*3.14*r*(height+r);
        System.out.println("area of Cylinder is "+area);
    }
}
class Ractangle extends Square{
    void area(){
        float length=2;
        float breadth=3;
        double area=length*breadth;
        System.out.println("area of Square is "+area);
    }
}

public class TestShape {
    public static void main(String[] args) {
        Shapes[] array=new Shapes[4];
        Shapes shapes;

        shapes=new Circle();
        array[0]=shapes;

        shapes=new Square();
        array[1]=shapes;

        shapes=new Cylinder();
        array[2]=shapes;

        shapes=new Ractangle();
        array[3]=shapes;

        for (Shapes element:array) {
            element.area();
        }
    }
}

