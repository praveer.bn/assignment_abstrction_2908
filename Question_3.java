package week3.assignment_abstarction;

import java.util.Random;

class Medicine{
    public void displayLable(){
        System.out.println("the company name is stark industries");
        System.out.println("Address:pune");
    }}
class Tablet extends Medicine{
        public void displayLable(){
        super.displayLable();
            System.out.println("Story in cold place");
        }
     }
class Syrup extends Medicine{
    public void displayLable(){
        super.displayLable();
        System.out.println("Syrup: take only 5ml");
    }
}
class Ointment extends Medicine{
    public void displayLable(){
        super.displayLable();
        System.out.println("Ointment is only for external");
    }
}


public class Question_3 {
    public static void main(String[] args) {
        Medicine[] array=new Medicine[10];
        Medicine medicine;
        for (int i=0;i< array.length;i++) {
            Random random = new Random();
            int a = random.nextInt(3);
            switch (a) {
                case 0:
                    medicine = new Tablet();
                    array[i] = medicine;
                    break;
                case 1:
                    medicine = new Syrup();
                    array[i] = medicine;
                    break;
                case 2:
                    medicine= new Ointment();
                    array[i] = medicine;
                    break;
            }
        }
        for(int i=0;i<array.length;i++){
            if(array[i] instanceof Tablet){
                array[i].displayLable();
                System.out.println("*******************************");
            } else if (array[i] instanceof Syrup) {
                array[i].displayLable();
                System.out.println("*******************************");
             }else {
                array[i].displayLable();
                System.out.println("*******************************");
             }
        }

    }
}
