package week3.assignment_abstarction;
abstract class Vehicle{
    abstract void engine();
}
class Car extends Vehicle{
    @Override
    void engine() {
        System.out.println("car has good engine");
    }
}
class Truck extends Vehicle{
    @Override
    void engine() {
        System.out.println("truck has bad engine");
    }
}
public class Question_1 {
    public static void main(String[] args) {
        Car car = new Car();
        car.engine();
        Truck truck=new Truck();
        truck.engine();
    }
}
